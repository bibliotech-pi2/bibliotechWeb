class UserNotifierMailer < ApplicationMailer
	 default :from => 'bibliotech@bibliotech.solutions'
    # send a signup email to the user, pass in the user object that   contains the user's email address

    def send_email_to_scheduled(user, schedule, book)
        @user = user
        @schedule = schedule
        @book = book
        mail(
            to:  @user.email,
            subject: 'Bibliotech - Livro Agendado: '+@book.title)
    end
end
