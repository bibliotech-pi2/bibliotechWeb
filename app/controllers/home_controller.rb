class HomeController < ApplicationController

	def index
		if logged_in?
			@books = Book.all
			if is_admin?
				@loans_finish = Loan.where("real_return_date IS NOT NULL")
				@loans_open = Loan.where(real_return_date: nil)
				@loans_late = []
				if @loans_open
					@loans_open.each do |loan|
						if Time.now > loan.return_date
							@loans_late << loan
						end
					end
				end
			else
				@loans_finish = current_user.loans.where("real_return_date IS NOT NULL")
				@loans_open = current_user.loans.where(real_return_date: nil)
				@loans_late = []
				if @loans_open
					@loans_open.each do |loan|
						if Time.now > loan.return_date
							@loans_late << loan
						end
					end
				end
			end
		else
			redirect_to login_path
		end
	end
end
