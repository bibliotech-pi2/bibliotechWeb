class SchedulesController < ApplicationController
	before_filter :autorized_user

	def new 
		@schedule = Schedule.new
		@book = Book.find(params[:id])
	end

	def create
		@book = Book.find_by_id(params[:id])
		if @book && !user_already_scheduled_book?(@book)
			@schedule = Schedule.new(book_id: @book.id, user_id: current_user.id)
			if scheduled?(@book.id)
				save_schedule('Agendamento criado, aguarde para saber o dia de pegar')
			else
				if @book.available
					@schedule.start_date = Time.zone.now + 1.day
					@schedule.end_date = @schedule.start_date + 15.days
					save_schedule('O agendamento foi criado e você deve pegá-lo até: ' + @schedule.start_date.to_s)
				else
					save_schedule('Agendamento criado, aguarde para saber o dia de pegar')
				end
			end
			UserNotifierMailer.send_email_to_scheduled(current_user, @schedule, @book).deliver_now
		else
			flash[:warning] = 'Não foi possível agendar o livro'
			redirect_to books_path
		end
	end

	def index
		@schedules = Schedule.where(user_id: current_user.id)
	end

	def destroy
		@schedule_dest = Schedule.find_by_id!(params[:id])
		if @schedule_dest && @schedule_dest.user_id == current_user.id
			book_id = @schedule_dest.book_id
			if @schedule_dest.destroy && book_id
				flash[:success] = 'Agendamento cancelado com sucesso'
				schedule = Schedule.find_by_book_id(book_id)
				if !schedule.nil? && schedule.start_date.nil? && schedule.end_date.nil?
					schedule.update_attributes(start_date: Time.zone.now + 1.day, end_date: Time.zone.now + 16.day)
				end
			else
				flash[:warning] = 'Não foi possível cancelar o agendamento'
			end
		end
		redirect_to schedules_path
	end

	private 
		def schedule_params
			params.require(:schedule)
		end

		def scheduled?(book_id)
			@schedule_valid = Schedule.find_by_book_id(book_id)
			!@schedule_valid.nil?
		end

		def save_schedule(msg)
			if @schedule.save
				redirect_to schedules_path
				flash[:success] = msg
			else
				redirect_to @book
				flash[:warning] = 'Não foi possível agendar o livro'
			end
		end
end