class BooksController < ApplicationController
	before_filter :autorized_user
	before_action :set_book, only: [:show, :edit, :update, :destroy]
	helper_method :is_admin?

	def index
		search_book(params[:book_search])
		@book_search = params[:book_search]
	end

	def new
		if is_admin?
			@book = Book.new
		else
			redirect_to books_path
			flash[:warning] = "Você não pode acessar essa página"
		end
	end

	def create
		if is_admin? 
			@book = Book.new(book_params)
			@book.available = true
			case_book = Case.find_by(book_id: nil)
			if !case_book.nil? && enough_cases?
				@book.case_id = case_book.id
				if @book.save
					case_book.update_attribute(:book_id, @book.id)
					flash[:success] = "Livro criado com sucesso"
					redirect_to books_path
				else
				   render :new
				end
			else
				flash[:warning] = "Não foi possível criar o livro, pois não existe uma case disponível."
			end
		else
			redirect_to books_path
			flash[:warning] = "Você não pode realizar essa ação"
		end
	end

	def show
		unless @book.condition_deactivate 
			@schedules = @book.schedules
		else
			redirect_to books_path
		end
	end

	def edit
		if is_admin? && !@book.condition_deactivate
			@selected_year = @book.year
		else
			redirect_to books_path
			flash[:warning] = "Você não pode acessar essa página"
		end
	end
	
	def update
		if is_admin? && !@book.condition_deactivate
			if @book.update(book_params)
				flash[:success] = "Livro atualizado com sucesso"
				redirect_to books_path
			else
				render :edit
			end
		else
			redirect_to books_path
			flash[:warning] = "Você não pode realizar essa ação"
		end
	end

	def destroy
		if is_admin?
			@book = Book.find(params[:id])
	   		if @book.present?
	   			case_book = Case.find(@book.case_id)
	   			if !case_book.nil?
	   				case_book.update_attribute(:book_id, nil)
	   			end
      		@book.update_attribute(:condition_deactivate, true)
      		@book.update_attribute(:case_id, nil)
      		flash[:success] = "Livro apagado com sucesso"
				redirect_to books_url
	    	end
	    else
			redirect_to books_path
			flash[:warning] = "Você não pode realizar essa ação"
		end
	end

	private 
	def set_book
		@book = Book.find(params[:id])
	end

	def book_params
		params.require(:book).permit(:author_one, :author_two, :author_three, :title,
		 :subtitle, :publisher, :year, :city, :edition, :volume, :pages, :ISBN, :barcode)
	end

	def search_book(book_search)
		if !book_search.blank?
			@books = Book.where(condition_deactivate: false).search_full_text("#{book_search}")
		else
			@books = Book.where(condition_deactivate: false)
		end
	end

	def enough_cases?
		cases_count = Case.all.count
		books_count = Book.all.count

		if books_count >= cases_count
			false
		else
			true
		end
	end
end
