class SessionsController < ApplicationController
	before_filter :autorized_user, :only => [:destroy]

    def new
        if logged_in?
            flash[:info] = "Você já está logado! Para entrar com uma conta diferente você precisa sair"
            redirect_to root_path
        end
    end

   def create
        @user = User.where(" email = ?", params[:session][:email]).first
        if @user && @user.authenticate(params[:session][:password])
            log_in @user
            flash[:success] = "Logado com sucesso "+@user.name
            redirect_to root_path
        else
            flash.now[:warning]= "Usuário ou senha inválidos"
            render "new"
        end
    end

    def destroy
        log_out
        redirect_to "/login"
    end
end
