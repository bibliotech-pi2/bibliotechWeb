class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  include ScheduleHelper

  helper_method :is_admin?, :current_user_loaned?

  def autorized_user
    unless logged_in?
        flash[:info] = "Não foi possível, você precisa estar logado"
        redirect_to login_path
    end
  end

  # Search if the user that loaned the book is the current user
  def current_user_loaned?(book)
    @loan = Loan.find_by(book_id: book, real_return_date: nil)
    if @loan && current_user
      if current_user.id == @loan.user_id
        true
      else
        false
      end
    end
  end

  def is_admin?
    if current_user
      if current_user.id == 1
        true
      end
    end
  end
end
