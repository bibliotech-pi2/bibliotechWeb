class UsersController < ApplicationController
	before_filter :autorized_user, :except => [:new, :create]
	before_action :set_user, only: [:show, :update, :destroy, :edit, :update_password]
	
	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			redirect_to login_path
			flash[:success] = "Usuário criado com sucesso"
		else
			render :new
		end
	end

	def show
		unless current_user.id == @user.id || is_admin?
			redirect_to user_path current_user
		end 
	end

	def index
		search_user(params[:users_search])
		@users_search = params[:users_search]
	end 

	def edit
		if current_user != @user
			redirect_to edit_user_path current_user
		else
			unless @user
				flash.now[:warning] = "Não foi encontrar o usuário"
				redirect_to users_path
			end
		end
	end

	def update
		if @user.update(user_params_update)
			flash.now[:success] = "Atualizado com sucesso"
			redirect_to root_path
		else
			render :edit
		end
	end

	def update_password
		if  @user.authenticate(params[:user][:older_password])
			if @user.id == current_user.id
				change_older_password
				flash[:success] = "Senha alterada com sucesso"
			else
				render :edit
				flash.now[:warning] = "Não foi possível atualizar a senha"
			end
		else
			flash.now[:warning] = "Senha antiga incorreta"
			render :edit
		end
	end

	def destroy
		if current_user.id == @user.id || is_admin?
			if @user.destroy
				flash.now[:success] = "Perfil removido com sucesso"
			else
				flash.now[:danger] = "Não foi possível remover seu perfil"
			end
			if is_admin?
				redirect_to users_path
			else
				log_out 
				redirect_to login_path
			end
		end
	end

	private 
	def set_user
		@user = User.find_by_id(params[:id])
	end

	def user_params
		params.require(:user).permit(:name, :cpf, :matricula, :email, :telefone,
			:dataNascimento, :endereco, :numero, 
			:complemento, :cep, :password,
			:password_confirmation,:email_confirmation)
	end

	def user_params_update
		params.require(:user).permit(:name, :cpf, :matricula, :telefone,
			:dataNascimento, :endereco, :numero, 
			:complemento, :cep)
	end

	def params_update_password
        params.require(:user).permit( :older_password, :password, :password_confirmation)
    end

	def change_older_password
		if @user.update_attributes(params_update_password)
			flash[:success] = 'Senha atualizada com sucesso'
			redirect_to root_path
		else
			render :edit
			flash.now[:warning] = 'Não foi possível atualizar a senha'
		end
	end

	def search_user(user_search)
		if is_admin?
			unless user_search.blank?
				@users = User.search_full_text("#{user_search}")
			else
				@users = User.all
			end
		else 
			flash[:warning] = "Você não tem permissão para realizar essa ação"
			redirect_to root_path
		end 
	end
end
