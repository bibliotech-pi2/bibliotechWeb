class LoansController < ApplicationController
	before_action :set_loan, only: [:show, :edit, :update, :return_book]

	def index
		if is_admin?
			@open_loans = Loan.where(real_return_date: nil)
			@close_loans = Loan.where("real_return_date IS NOT NULL")
		else
			if logged_in?
				@open_loans = current_user.loans.where(real_return_date: nil)
				@close_loans = current_user.loans.where("real_return_date IS NOT NULL")
			else
				flash[:warning] = "Você precisa estar logado."
			end
		end
	end

	private 
		def set_loan
			@loan = Loan.find_by(book_id: params[:id], real_return_date: nil)
		end
		
end
