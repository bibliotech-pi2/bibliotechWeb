class User < ApplicationRecord
	attr_accessor :email_confirmation, :older_password
	has_many :schedules, :dependent => :destroy
	has_many :loans, :dependent => :destroy
	
	#Bcrypt encrypt
    has_secure_password :validations => false

	#name
    validates_length_of :name,
				  		:within => 3..50,
				  		:too_short => "Nome tem que ter no mínimo 3 caracteres",
				  		:too_long => "Nome tem que ter no máximo 50 caracteres"
    validates :name, :presence => { message: "Nome Completo não pode ser vazio" }


	#email
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(?:\.[a-z\d\-]+)*\.[a-z]+\z/i
	validates :email, :uniqueness => { message:"Este e-mail já está em uso"},
            :presence => { message: "E-mail não pode ser vazio" },
	          length: { maximum: 50},
	          format: { with: VALID_EMAIL_REGEX, message: "Insira um e-mail válido"}, 
	          on: :create

	#password
    validates_length_of :password,
			            :within => 6..15,
			            :too_short => 'Senha tem que ter no mínimo 6 caracteres',
			            :too_long => 'Senha tem que ter no máximo 15 caracteres',
			            on: :create

	#mobile phone
	validates :telefone, :numericality => {:message => 'Telefone deve ter somente números e sem espaços'}
	validates_length_of :telefone,
				  		:within => 11..15,
				  		:too_short => 'Telefone tem que ter no mínimo 11 caracteres!',
				  		:too_long => 'Telefone tem que ter no máximo 15 caracteres!'

	#cpf
	validates_length_of :cpf, :is => 11 , :message => 'CPF inválido'
	validates_numericality_of :cpf, :message => 'CPF deve possuir apenas números'
	validates :cpf, :uniqueness => {message: "Este CPF já está sendo usado"}
    validates :cpf, :presence => { message: "CPF não pode ser vazio" }


	#matricula
	validates_numericality_of :matricula, :message => 'Matrícula deve possuir apenas números'
	validates :matricula, :uniqueness => {message: "Esta matrícula já está sendo usada"}
    validates :matricula, :presence => { message: "Matrícula não pode ser vazia" }


	#numero
	validates_numericality_of :numero, :message => 'Número deve possuir apenas números'

	#CEP
	validates_numericality_of :cep, :message => 'CEP deve possuir apenas números'
	validates_length_of :cep, :is => 8 , :message => 'CEP inválido'

	#Endereço
	validates_length_of :endereco,
				  		:within => 3..50,
				  		:too_short => "Endereço tem que ter no mínimo 3 caracteres",
				  		:too_long => "Endereço tem que ter no máximo 50 caracteres"

	#complemento
	validates :complemento, length: { maximum: 20, message: "Complemento deve ter no máximo 20 caracteres" }

	#Data Nascimento
	validates :dataNascimento, :presence => {:message => 'Data de Nascimento não pode ser vazia'}

	#password_confirmation
	validates_confirmation_of :password, :message => "As senhas devem ser iguais na confirmação"

    #email_confirmation
	validates_confirmation_of :email, :message => "Os e-mails devem ser iguais na confirmação",
	                                  on: :create

	#older_password
	validates :older_password, length: {minimum: 6,
                               	        message: 'Senha antiga deve possuir no mínimo 6 caracteres'},
                               on: :update_password

    include PgSearch
	pg_search_scope :search_full_text, 
	                :against => [:name, :cpf, :email, :matricula], 
	                :using => { 
			                	:tsearch => {:prefix => true, :any_word => true}
			                   }
	has_many :loans, :dependent => :destroy
end
