class Case < ApplicationRecord
	has_one :book
	belongs_to :book

  validates_uniqueness_of :x_position, :scope => [:y_position]
  validates_uniqueness_of :book_id, :allow_nil => true
  validates :x_position, :presence => {message: "Posição X não informada!"}
  validates :y_position, :presence => {message: "Posição Y não informada!"}
end