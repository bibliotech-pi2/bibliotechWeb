class Book < ApplicationRecord
	has_many :schedules, :dependent => :destroy
	has_one :loan
	has_one :case
	belongs_to :case
	#title
	validates_length_of :title,
						:within => 1..100, 
						:too_short => "Título tem que ter no mínimo 1 caracter",
				  		:too_long => "Título tem que ter no máximo 100 caracteres"
	validates :title, :presence => { :message => "Informe o título do livro" }
	
	#subtitle
	validates :subtitle, length: { maximum: 100, message: "Subtítulo tem que ter no máximo 100 caracteres"}

	#author_one
	validates_length_of :author_one,
						:within => 3..100, 
						:too_short => "Primeiro autor tem que ter no mínimo 3 caracteres",
				  		:too_long => "Primeiro autor tem que ter no máximo 100 caracteres"
	validates :author_one, :presence => { :message => "Informe o nome do primeiro autor"}
	
	
	#author_two
	validates :author_two, length: { maximum: 100, message: "Segundo autor tem que ter no máximo 100 caracteres"}

	#author_three
	validates :author_three, length: { maximum: 100, message: "Terceiro autor tem que ter no máximo 100 caracteres"}

	#publisher
	validates_length_of :publisher,
						:within => 2..20, 
						:too_short => "Editora tem que ter no mínimo 2 caracteres",
				  		:too_long => "Editora tem que ter no máximo 20 caracteres"
	validates :publisher, :presence => { :message => "Informe a editora do livro"}

	#year
	validates_length_of :year,
						:within => 4..4, 
						:too_short => "Ano tem que ter no mínimo 4 caracteres",
				  		:too_long => "Ano tem que ter no máximo 4 caracteres"
	validates :year, :presence => {:message => "Informe o ano do livro"}
	
	#city
	validates_length_of :city,
						:within => 3..20, 
						:too_short => "Cidade tem que ter no mínimo 3 caracteres",
				  		:too_long => "Cidade tem que ter no máximo 20 caracteres"
	validates :city, :presence => { :message => "Informe o local do livro"}
	

	#edition
	validates_length_of :edition,
						:within => 1..3, 
						:too_short => "Edição tem que ter no mínimo 1 caracter",
				  		:too_long => "Edição tem que ter no máximo 3 caracteres"
	validates :edition, :presence => { :message => "Informe a edição do livro"}

	#volume
	validates :volume, length: { maximum: 3, message: "Volume tem que ter no máximo 3 caracteres"}

	#pages
	validates_length_of :pages,
						:within => 1..5, 
						:too_short => "Página tem que ter no mínimo 1 caracteres",
				  		:too_long => "Página tem que ter no máximo 5 caracteres"
	validates :pages, :presence => { :message => "Informe as páginas do livro"}

	#isbn
	validates_length_of :ISBN,
						:within => 1..13, 
						:too_short => "ISBN tem que ter no mínimo 1 caracteres",
				  		:too_long => "ISBN tem que ter no máximo 13 caracteres"
	validates :ISBN, :presence => {:message => "Informe o ISBN do livro" }

	#barcode
	validates_length_of :barcode,
						:within => 1..15, 
						:too_short => "Código de barras tem que ter no mínimo 1 caracteres",
				  		:too_long => "Código de barras tem que ter no máximo 15 caracteres"
	validates :barcode, :presence => { :message => "Informe o código de barras do livro"}
	
	include PgSearch
	pg_search_scope :search_full_text, 
	                :against => [:title, :author_one, :publisher], 
	                :using => { 
	                	:tsearch => {:prefix => true, :any_word => true}
	                    }
end
