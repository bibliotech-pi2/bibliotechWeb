module ScheduleHelper
	def user_already_scheduled_book?(book)
		@schedule = Schedule.where(book_id: book, user_id: current_user.id).first
		!@schedule.nil?
	end
end
