# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

## Deixe sempre o admin como o primeiro usuário a ser criado. Obrigada.
User.create(:name => "Admin", 
	        :cpf => "00000000000", 
	        :matricula => "100130689", 
	        :email => "admin@admin.com",
	        :telefone => "61971985577",
			:dataNascimento => "2015-02-02", 
			:endereco => "Admin Home", 
			:numero => "3", 
			:complemento => "Home", 
			:cep => "71916000", 
			:password => "123456",
			:password_confirmation => "123456",
			:email_confirmation => "admin@admin.com")

User.create(:name => "Murilo Duarte", 
	        :cpf => "03993874569", 
	        :matricula => "110130689", 
	        :email => "test@test.com",
	        :telefone => "61981985577",
			:dataNascimento => "2016-02-02", 
			:endereco => "Rua 21 norte", 
			:numero => "3", 
			:complemento => "Apt 1703", 
			:cep => "71916000", 
			:password => "123456",
			:password_confirmation => "123456",
			:email_confirmation => "test@test.com")

User.create(:name => "Karine Valença", 
	        :cpf => "03993874567", 
	        :matricula => "01201140465011", 
	        :email => "karine@karine.com",
	        :telefone => "61981985577",
			:dataNascimento => "2016-02-02", 
			:endereco => "Rua 21 norte", 
			:numero => "3", 
			:complemento => "Apt 1703", 
			:cep => "71916000", 
			:password => "123456",
			:password_confirmation => "123456",
			:email_confirmation => "karine@karine.com")

User.create(:name => "Iago Rodrigues", 
	        :cpf => "03993874500", 
	        :matricula => "01201140281018", 
	        :email => "iago006@hotmail.com",
	        :telefone => "61981985577",
			:dataNascimento => "2016-02-02", 
			:endereco => "Rua 21 norte", 
			:numero => "3", 
			:complemento => "Apt 1703", 
			:cep => "71916000", 
			:password => "123456",
			:password_confirmation => "123456",
			:email_confirmation => "iago@iago.com")

User.create(:name => "Pedro Rasp", 
	        :cpf => "03993874525", 
	        :matricula => "110130625", 
	        :email => "pedro@pedro.com",
	        :telefone => "61981985577",
			:dataNascimento => "2016-02-02", 
			:endereco => "Rua 21 norte", 
			:numero => "3", 
			:complemento => "Apt 1703", 
			:cep => "71916000", 
			:password => "123456",
			:password_confirmation => "123456",
			:email_confirmation => "pedro@pedro.com")

Case.create(:x_position => "0",
	:y_position => "0")

Case.create(:x_position => "0",
	:y_position => "4")

Case.create(:x_position => "1",
	:y_position => "2")

Case.create(:x_position => "2",
	:y_position => "1")

Case.create(:x_position => "2",
	:y_position => "3")