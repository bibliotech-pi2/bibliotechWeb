class AddRealReturnDateToLoans < ActiveRecord::Migration[5.0]
  def change
    add_column :loans, :real_return_date, :date
  end
end