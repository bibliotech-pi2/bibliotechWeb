class CreateLoans < ActiveRecord::Migration[5.0]
  def change
    create_table :loans do |t|
      t.datetime :loan_date
      t.datetime :return_date

      t.timestamps
    end
  end
end
