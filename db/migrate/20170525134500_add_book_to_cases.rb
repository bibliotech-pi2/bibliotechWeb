class AddBookToCases < ActiveRecord::Migration[5.0]
  def change
    add_reference :cases, :book, foreign_key: true
  end
end
