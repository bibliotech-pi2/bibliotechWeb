class AddCaseToBooks < ActiveRecord::Migration[5.0]
  def change
    add_reference :books, :case, foreign_key: true
  end
end
