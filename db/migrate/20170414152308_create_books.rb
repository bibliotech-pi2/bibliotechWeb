class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.string :author_one
      t.string :author_two
      t.string :author_three
      t.string :title
      t.string :subtitle
      t.string :publisher
      t.integer :year
      t.string :city
      t.integer :edition
      t.integer :volume
      t.integer :pages
      t.string :ISBN
      t.string :barcode

      t.timestamps
    end
  end
end
