class CreateCases < ActiveRecord::Migration[5.0]
  def change
    create_table :cases do |t|
      t.integer :x_position
      t.integer :y_position

      t.timestamps
    end
  end
end
