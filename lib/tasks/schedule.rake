namespace :schedule  do
	task :destroy => :environment do
		@schedules =  Schedule.where("start_date IS NOT NULL")
		@schedules.each do |schedule|
			unless schedule.start_date > Time.now
				book_id = schedule.book_id
				if schedule.destroy && book_id
					schedule = Schedule.find_by_book_id(book_id)
					if !schedule.nil? && schedule.start_date.nil? && schedule.end_date.nil?
						schedule.update_attributes(start_date: Time.zone.now + 1.day, end_date: Time.zone.now + 16.day)
					end
				end
			end
		end 
	end
end
	