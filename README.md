# Software de controle de Bibliotecas

## Pré-requisitos:

  - Ruby 2.4.0
  - rails 5.0.1
  - postgreSql 9.6

---------------  

## Iniciando aplicação manualmente:

#### Clonar repositório:

```sh
$ cd
$ git clone https://muriloduarte@gitlab.com/bibliotech-pi2/bibliotechWeb.git
```

#### [Instalar Dependências](https://gorails.com/setup/ubuntu/16.04):

  - Ruby 2.4.0
  - rails 5.0.1
  - postgreSql 9.6

```sh
$ cd bibliotechWeb
```

#### Iniciando dependências do rails:

```sh
$ bundler install 
```

#### Iniciando o banco de dados:

verifique se [declarou as variáveis](https://gitlab.com/bibliotech-pi2/bibliotechWeb#definindo-vari%C3%A1veis-de-ambiente) na sua aplicação.

```sh
$ rake db:create db:migrade db:seed
```

#### Iniciando aplicação:

```sh
$ rails s
```

---------------

## Iniciando aplicação com Docker:

#### Clonar repositório:

```sh
$ cd
$ git clone https://muriloduarte@gitlab.com/bibliotech-pi2/bibliotechWeb.git
```

#### Montar Docker:

O primeiro passo é montar o ambiente de desenvolvimento. Está desenhado para funcionar automaticamente no Docker

* Instale o docker: https://docs.docker.com/engine/installation/

* Instale o docker-compose: https://docs.docker.com/compose/install/

```sh
$ cd
$ cd bibliotechWeb
```

```sh
$ docker-compose build
```
#### Montar banco de dados:

```sh
$ docker-compose run web rake db:create db:migrate db:seed
```

#### Montar Aplicação:

Depois da 1ª configuração, utilizar sempre que quiser levantar a aplicação, levando em conta as alterações de dependências.

```sh
$ docker-compose up --build
```

Sempre que quiser levantar a aplicação, sem alterações nas dependências (Gemfile).

```sh
$ docker-compose up
```

Isso vai iniciar o Wordpress na porta 8080 da sua máquina local, se tudo estiver funcionando. Acessar com "http://localhost"

### Comandos Auxiliares Docker:

Removendo todas as imagens de containers do docker da sua máquina.
```sh
$ docker rmi -f $(docker images -a -q)
```

```sh
$ docker rmi $(docker images -q)
```

Removendo todos os containers docker da sua máquina. 
```sh
$ docker rmi -f $(docker ps -a -q)
```

```sh
$ docker rm $(docker ps -a -q)
```

Problemas com chache depois de montar aplicação com o docker:

```sh
$ rake tmp:cache:clear
```
---------------

# Apêndice

## Tema Usado
https://github.com/mwlang/gentelella-rails

## Definindo Variáveis de Ambiente:

Atribuição de variáveis de ambiente deve ser feita com a gem figaro:
```sh
$ bundle exec figaro install
```

Abra o arquivo "/bibliotechWeb/config/application.yml" e declare as variáveis.

> Variáveis que precisam ser declaradas:
- SECRET_KEY_BASE - (chave secreta utilizada só em modo de produção)
- DB_LOGIN - (login do postgresql em modo de produção)
- DB_PSW - (senha do postgresql em modo de produção)
- DB_HOST: (instância do banco em produção, pode ser "localhost" ou "ip da maquina do banco")
>
- DB_HOST_DEV - (instância do banco  em modo de desenvolvimento, pode ser "localhost" ou "ip da maquina do banco")
- DB_DATA_DEV - (nome do banco de dados em modo de desenvolvimento)
- DB_LOGIN_DEV - (login do postgresql em modo de desenvolvimento)
- DB_PSW_DEV - (senha do postgresql em modo de desenvolvimento)

#### Exemplo de uso:

> Variáveis que precisam ser declaradas:
- SECRET_KEY_BASE: 2fa9bd0413c8973afbc794d5f8abce5ddebe4dcbd1f5167a141cf3198d92931e6e238d0779d7fe509c381a71e5fcb886f637165a7eea446cc7b7b24335289950
- DB_LOGIN: bibliotech
- DB_PSW: bibliotech
- DB_HOST: 00.000.00.00
>
- DB_HOST_DEV: localhost
- DB_DATA_DEV: bibliotech
- DB_LOGIN_DEV: bibliotech
- DB_PSW_DEV: bibliotech

## Serializer

Esse projeto possui uma dependência de sempre criar um serializer correspondente a modelo que estiver criando! 
Esse serializer permite que controlemos quais dados das modelos serão disponibilizados via JSON na API.

```sh
$ rails g serializer "nome da modelo *Sem aspas"
```
---------------

## Whenever Gem

A gem [Whenever](https://github.com/javan/whenever) foi usada para automatizar o uso de crontab

```sh
$ whenever --update-crontab --set environment=development
```



---------------

## Comandos Servidor Produção:

#### Pré-requisitos:

 - Nginx
 - Passenger
 - git
 - rails 
 - ruby

#### Comandos NGINX/PASSENGER

```sh
$ passenger-config restart-app
```

```sh
$ sudo systemctl stop nginx
$ sudo systemctl start nginx
$ sudo service nginx restart
```

#### DB comandos básicos:

  - RAILS_ENV=production bundle exec rake db:drop db:create db:migrate db:seed
  - rake assets:precompile RAILS_ENV=production

#### Deploy baseado:

- [postgree](https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-ruby-on-rails-application-on-centos-7)
- [centos7](http://blog.0e1dev.com/2016/11/23/Subindo-uma-aplicacao-Ruby-on-Rails-5-em-um-servidor-Linux-AWS-DigitalOcean-Linode-entre-outros/)
- [update centos7](https://www.digitalocean.com/community/tutorials/how-to-add-swap-on-centos-7)
