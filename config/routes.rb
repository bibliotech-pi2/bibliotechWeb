Rails.application.routes.draw do
	root 'home#index'

	#user
	patch 'user/:id/edit/password'=> 'users#update_password', :as => 'alterar_senha'
	get 'users/:id/delete', to: 'users#destroy', :as => 'delete_user'
	get 'users' => 'users#index', :as => 'users'
	post 'users' => 'users#index', :as => 'users_post'
	post 'users/create' => 'users#create', :as => 'users_create'

	#sessions
	get    'login'   => 'sessions#new'
	post   'login'   => 'sessions#create'
	get 'logout', to: 'sessions#destroy', :as => 'logout'

	#books
	get 'books' => 'books#index', :as => 'books'
	post 'books' => 'books#index', :as => 'books_post'
	post 'books/create' => 'books#create', :as => 'books_create'
	get 'books/:id/delete', to: 'books#destroy', :as => 'delete_book'

	#loans
	get 'loans/create/:id' => 'loans#create', :as => 'loans_create'
	get 'loans/return_book/:id' => 'loans#return_book', :as => 'loans_return_book'
	get 'loans' => 'loans#index', :as => 'loans'

	#schedules
	get 'schedules/create/:id' => 'schedules#create', :as => 'schedules_create'
	get 'schedules' => 'schedules#index', :as => 'schedules'
	get 'schedule/:id/delete', to: 'schedules#destroy', :as => 'delete_schedule'


	resources :books, except:[:index, :create]
	resources :users, except:[:index, :create]
	resources :sessions
end
