set :output, 'log/whenever.log'

every 1.minute do 
	rake "schedule:destroy"
end

# 1.minute 1.day 1.week 1.month 1.year is also supported
#:hour 
# whenever --update-crontab --set environment=development